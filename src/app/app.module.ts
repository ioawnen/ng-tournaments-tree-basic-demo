import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgTournamentTreeModule } from 'ng-tournament-tree';

import { AppComponent } from './app.component';
import { MatchComponent } from './match/match.component';

@NgModule({
  declarations: [
    AppComponent,
    MatchComponent
  ],
  imports: [
    BrowserModule,
    NgTournamentTreeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
