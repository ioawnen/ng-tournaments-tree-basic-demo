import { Component } from '@angular/core';
import { NgttTournament } from 'ng-tournament-tree';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public myTournamentData: NgttTournament = {
    rounds: [
      {
        type: 'Winnerbracket',
        matches: [['bob', 'ian'], ['chris', 'jordan']]
      },
      {
        type: 'Final',
        matches: [['ian', 'chris']]
      }
    ]
  };
}
